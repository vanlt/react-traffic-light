import React, {Component} from 'react';
import "./Traffic_light.css";
import classNames from "classnames";
import MessageTf from "./Message";

const RED = 0;
const ORANGE = 1;
const GREEN = 2;

class TrafficLight extends React.Component{
    constructor(){        
        super();
        this.state = {
            currentColor : RED
        }

         setInterval(() => {
            //alert(this.currentColor);
            this.setState({
                currentColor : this.getNextColor(this.state.currentColor)
            });
         },1000);
    }

    getNextColor(color){
        switch(color){
            case RED:
                return ORANGE;
            case ORANGE:
                return GREEN;
            default:
                return RED;
        }
    }
    render(){
        const {currentColor} = this.state;
        return(
            <div>
                <div className="Traffic_light">
                    <div className={classNames('bulb', 'red', {active: currentColor === RED})}></div>
                    <div className={classNames('bulb', 'orange', {active: currentColor === ORANGE})}></div>
                    <div className={classNames('bulb', 'green', {active: currentColor === GREEN})}></div>
                </div>
                <MessageTf currentMessage = {currentColor} />
            </div>
        );
    }
}

export default TrafficLight;
